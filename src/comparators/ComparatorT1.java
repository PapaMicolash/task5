package comparators;

import details.Detail;

import java.util.Comparator;

public class ComparatorT1 implements Comparator<Detail> {
    @Override
    public int compare(Detail o1, Detail o2) {
        if (o1.getT1() < o2.getT1()) {
            return  -1;
        } else if (o1.getT1() > o2.getT1()) {
            return 1;
        } else {
            return 0;
        }
    }
}
