package comparators;

import details.Detail;

import java.util.Comparator;

public class ComparatorT2 implements Comparator<Detail> {
    @Override
    public int compare(Detail o1, Detail o2) {
        if (o1.getT2() >= o2.getT2()) {
            return  -1;
        } else  {
            return 1;
        }
    }
}
