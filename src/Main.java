import details.Detail;
import details.ListOfDetail;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        // first: enter name of file
	    String fileName = "src/input.txt";

	    // second: creating of list for details and working of this
        ArrayList<Detail> details = new ArrayList<>();
        ListOfDetail firstList = new ListOfDetail(details);

        firstList.getBufferReaderOfFile(fileName);

        System.out.println(firstList.toString());
        firstList.getCompareSortDetails();

        for (int i = 0; i < firstList.getDetails().size(); i++) {
            System.out.println(i+1 +"" + firstList.getDetails().get(i));
        }

        System.out.println();

        System.out.println(firstList.getOptimalTime());

    }
}
