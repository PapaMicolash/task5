package details;

import comparators.ComparatorT1;
import comparators.ComparatorT2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ListOfDetail {

    ArrayList<Detail> details = new ArrayList<>();

    public ListOfDetail(ArrayList<Detail> details) {
        this.details = details;
    }

    public ListOfDetail() {
    }

    public ArrayList<Detail> getDetails() {
        return details;
    }

    public void setDitails(ArrayList<Detail> details) {
        this.details = details;
    }

    public void getBufferReaderOfFile(String fileName) {
        try {
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            try {
                String string;
                Detail tempDetail;
                while((string = in.readLine()) != null) {
                    tempDetail = getDetailFromString(string);
                    details.add(tempDetail);
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Detail getDetailFromString(String string) {

        String[] stringSplit = string.split(" ");

        switch (stringSplit[0]) {
            case "Bolt":
                Bolt bolt = new Bolt(stringSplit[0], Integer.parseInt(stringSplit[1]), Integer.parseInt(stringSplit[2]));
                return bolt;

            case "Nut":
                Nut nut = new Nut(stringSplit[0], Integer.parseInt(stringSplit[1]), Integer.parseInt(stringSplit[2]));
                return nut;

            case "Sleeve":
                Sleeve sleeve = new Sleeve(stringSplit[0], Integer.parseInt(stringSplit[1]), Integer.parseInt(stringSplit[2]));
                return sleeve;

            case "Spring":
                Spring spring = new Spring(stringSplit[0], Integer.parseInt(stringSplit[1]), Integer.parseInt(stringSplit[2]));
                return spring;

            default:
                System.out.println("unknown detail");
                return null;
        }
    }

    public void getCompareSortDetails() {
        ArrayList<Detail> detailsT1 = new ArrayList<>();
        ArrayList<Detail> detailsT2 = new ArrayList<>();
        ComparatorT1 comparatorT1 = new ComparatorT1();
        ComparatorT2 comparatorT2 = new ComparatorT2();

        for (int i = 0; i < details.size(); i++) {
            if (details.get(i).getT1() <= details.get(i).getT2()) {
                detailsT1.add(details.get(i));
            } else if (details.get(i).getT2() <= details.get(i).getT1()) {
                detailsT2.add(details.get(i));
            }
        }

        detailsT1.sort(comparatorT1);
        detailsT2.sort(comparatorT2);

        detailsT1.addAll(detailsT2);

        details.clear();
        details.addAll(detailsT1);
    }

    public int getOptimalTime() {
        int time = 0;

        for (int i = 0; i < details.size(); i++) {
            time += details.get(i).getT2();
            time += (details.get(i).getT1() - details.get(i).getT2());
            //time += details.get(i).getT1();
        }

        time += details.get(details.size() - 1).getT2();

        //optimal downtime

        /*for (int i = 0; i < details.size() - 1; i++) {
            time -= details.get(i).getT2();
        }*/

        return time;
    }

    @Override
    public String toString() {
        return "ListOfDetail{" + "\n" +
                "details=" + details + "\n" +
                '}';
    }
}
