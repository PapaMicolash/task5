package details;

public class Bolt extends Detail {

    // *болт

    public Bolt(String name, int t1, int t2) {
        super(name, t1, t2);
    }

    public Bolt() {
    }

    @Override
    public String toString() {
        return "\n"+"Bolt{" +
                "name='" + name + '\'' +
                ", t1=" + t1 +
                ", t2=" + t2 +
                '}';
    }
}
