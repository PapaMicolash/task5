package details;

public class Sleeve extends Detail {

    // *металлическая втулка

    public Sleeve(String name, int t1, int t2) {
        super(name, t1, t2);
    }

    public Sleeve() {
    }

    @Override
    public String toString() {
        return "\n"+"Sleeve{" +
                "name='" + name + '\'' +
                ", t1=" + t1 +
                ", t2=" + t2 +
                '}' ;
    }
}
