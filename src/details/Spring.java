package details;

public class Spring extends Detail {

    // *пружина

    public Spring(String name, int t1, int t2) {
        super(name, t1, t2);
    }

    public Spring() {
    }

    @Override
    public String toString() {
        return "\n"+"Spring{" +
                "name='" + name + '\'' +
                ", t1=" + t1 +
                ", t2=" + t2 +
                '}' ;
    }
}
