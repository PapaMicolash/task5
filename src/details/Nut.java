package details;

public class Nut extends Detail {

    // *гайка

    public Nut(String name, int t1, int t2) {
        super(name, t1, t2);
    }

    public Nut() {
    }

    @Override
    public String toString() {
        return "\n"+ "Nut{" +
                "name='" + name + '\'' +
                ", t1=" + t1 +
                ", t2=" + t2 +
                '}' ;
    }
}
