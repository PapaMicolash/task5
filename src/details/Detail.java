package details;

import java.util.Objects;

public abstract class Detail {

    String name;
    int t1;
    int t2;

    public Detail(String name, int t1, int t2) {
        this.name = name;
        this.t1 = t1;
        this.t2 = t2;
    }

    public Detail() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getT1() {
        return t1;
    }

    public void setT1(int t1) {
        this.t1 = t1;
    }

    public int getT2() {
        return t2;
    }

    public void setT2(int t2) {
        this.t2 = t2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Detail detail = (Detail) o;
        return t1 == detail.t1 &&
                t2 == detail.t2 &&
                Objects.equals(name, detail.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, t1, t2);
    }

    @Override
    public String toString() {
        return "Detail{" +
                "name='" + name + '\'' +
                ", t1=" + t1 +
                ", t2=" + t2 +
                '}';
    }
}
